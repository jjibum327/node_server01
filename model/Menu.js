var createMenu = function() {
    console.log("▶▷▶▷ create menu");
    var arr_menu = [{id:'about', name:"About", url:"/about"},
                    {id:"tran1", name:"Transaction(입력)", url:"/tran1"},
                    {id:"tran2", name:"Transaction(조회)", url:"/tran2"}];
    var str_menu = '';
    arr_menu.forEach(function(element) {
        str_menu += '<a class="dropdown-item" href="'+element.url+'">'+element.name+'</a>';
    });
    return str_menu;
}
