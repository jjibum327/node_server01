var mongoose = require('mongoose');
mongoose.pluralize(null);
console.log("▶▷▶▷ create table schema");
var ProductSchema = new mongoose.Schema({
    item_name : String,
    item_desc : String,
    item_price : Number,
    updated_ad : {type: Date, default : Date.now},
});

module.exports = mongoose.model('Prod', ProductSchema);