var express = require('express');
var router = express.Router();
var mongoose = require('mongoose');
var Item = require('../model/Products');

//get all items
router.get('/', function(req, res, next) {
    Item.find(function(err,items) {
        if(err) return next(err);
        res.json(items);
    });
});

//single item
router.get('/:id', function(req,res,next) {
    Item.find({"item_name":req.params.id}, function(err,post) {
        if(err) return next(err);
        res.json(post);
    });
});

//get single item (POST)
router.post('/get', function(req,res,next) {
    console.log(req.body)

    if(req.body.item_name == null || req.body.item_name == '') {
        Item.find(function(err,items) {
            if(err) return next(err);
            res.json(items);
        });
    } else {
        Item.find({"item_name":req.body.item_name}, function(err,post) {
            if(err) return next(err);
            res.json(post);
        });
    }
});


//save item
router.post('/', function(req,res,next) {
    Item.create(req.body, function(err, post) {
        console.log(req.body);
        if(err) return next(err)
        res.json(post);
    });
});

//update
router.put('/:id', function(req,res,next) {
    Item.findByIdAndUpdate(req.params.id, res.body, function(err,post) {
        if(err) return next(err);
        releaseEvents.json(post);
    });
});

//delete
router.delete('/:id', function(req, res, next) {
    Item.findByIdAndRemove(req.params.id, res.body, function(err, post) {
        if(err) return next(err);
        res.json(post);
    });
});

module.exports = router;
