const express = require('express');
const router =express.Router();

const main = require('./main.js');
const about = require('./about.js');
const items = require('./items');
const tran1 = require('./tran1.js');
const tran2 = require('./tran2.js');

router.use('/', main);
router.use('/about', about);
router.use('/tran1', tran1);
router.use('/tran2', tran2);
router.use('/items', items);

module.exports = router;